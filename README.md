# Test `system-manager` on g5k

## Get a node

```
oarsub -I
```

## Become root

As `system-manager` needs to call `sudo`, we will do everyting as root.

```
sudo-g5k su
```

## Install nix

```
sh <(curl -L https://nixos.org/nix/install) --daemon
```

You might need to open a new shell after the installation (just running `bash` is fine).

## Enter the dev shell

```
nix --experimental-features 'nix-command flakes' develop
```

You should have `system-manager` available

## Build and switch to the desired configuration

```
system-manager switch --flake '.'
```

## Inspect the services

In the case of this example, we start a `nginx` service:

```
systemctl status nginx
```
