{ pkgs, ... }: {
  nixpkgs.hostPlatform = "x86_64-linux";
  services.nginx = {
    enable = true;
    # a minimal site with one page
    virtualHosts.default = {
      root = pkgs.runCommand "testdir" { } ''
        mkdir "$out"
        echo hello world > "$out/index.html"
      '';
    };
  };
  # networking.firewall.enable = false;
}

