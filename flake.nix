{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

    system-manager = {
      url = "github:GuilloteauQ/system-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, system-manager }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs { inherit system; };
    in
    {
      systemConfigs.default = system-manager.lib.makeSystemConfig {
        modules = [
          ./modules/web.nix
        ];
      };
      devShells.${system}.default = pkgs.mkShell {
        buildInputs = [
          system-manager.packages.${system}.system-manager
        ];
      };
    };
}
